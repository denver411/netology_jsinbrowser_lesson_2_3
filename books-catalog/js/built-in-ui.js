/* Данный JS код */

// Регулируем видимость карточки
function toggleCardVisible() {
    document.getElementById('content').classList.toggle('hidden');
    document.getElementById('card').classList.toggle('hidden');
}


document.getElementById('close').addEventListener('click', toggleCardVisible);

document.getElementById('content').addEventListener('click', (event) => {
    let target = null;
    if (event.target.tagName === 'LI') {
        target = event.target;
    }
    if (event.target.parentNode.tagName === 'LI') {
        target = event.target.parentNode;
    }

    if (target) {
        toggleCardVisible();
        document.getElementById('card-title').innerHTML = target.dataset.title;
        document.getElementById('card-author').innerHTML = target.dataset.author;
        document.getElementById('card-info').innerHTML = target.dataset.info;
        document.getElementById('card-price').innerHTML = target.dataset.price;
    }
});

// решение задания 3

let catalogNode = document.getElementById('content');
let bookDataXhr = new XMLHttpRequest();
bookDataXhr.open("GET", 'https://neto-api.herokuapp.com/book/', true);
bookDataXhr.send();
bookDataXhr.addEventListener("load", createCatalog);

function createCatalog() {
    let books = JSON.parse(bookDataXhr.responseText);
    catalogNode.innerHTML = '';
    books.forEach(element => {
        catalogNode.innerHTML += `<li data-title='${element.title}' data-author='${element.author.name}' data-info = '${element.info}' data-price = '${element.price}'><img src='${element.cover.small}'></li>`;
    });
}