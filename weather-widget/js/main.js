const request = new XMLHttpRequest();
request.addEventListener('load', onLoadEnd);
request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/weather', true);
request.send();

function onLoadEnd() {
  if (request.status === 200) {
    const response = JSON.parse(request.responseText);
    setData(response);
  }
}